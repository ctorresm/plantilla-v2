import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from "primeng/dialog";
import { TooltipModule } from "primeng/tooltip";
import { InputSwitchModule } from "primeng/inputswitch";
import { TableModule } from "primeng/table";
import { TabViewModule } from "primeng/tabview";
import { FileUploadModule } from "primeng/fileupload";
import { PanelModule } from 'primeng/panel';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
    imports: [
        CommonModule,
        CheckboxModule,
        DialogModule,
        TooltipModule,
        InputSwitchModule,
        TableModule,
        TabViewModule,
        FileUploadModule,
        PanelModule,
        AutoCompleteModule
    ],
    exports: [
        CheckboxModule,
        DialogModule,
        TooltipModule,
        InputSwitchModule,
        TableModule,
        TabViewModule,
        FileUploadModule,
        PanelModule,
        AutoCompleteModule
    ]
})

export class primeNgModule { }