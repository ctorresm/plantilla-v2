import { Representante } from "./Representante";

export class Proveedor {
  provRUC: string;
  provRazonSocial: string;
  provTipoProveedor: string;
  provDirecion: string;
  provDepartamento: string;
  provProvincia: string;
  provDistrito: string;
  provTelefono: string;
  provAnexo: string;
  provPaginaWeb: string;
  provAceptarTemino: string;
  maxAutoridad: Representante;
  representante: Representante;
}
