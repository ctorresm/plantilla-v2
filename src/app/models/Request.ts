import { Proveedor } from './Proveedor';
import { Representante } from './Representante';

export interface RequestRegistroProveedor{
    proveedor : Proveedor,
    representantes : Representante[]  
  }
  