export interface UserLoginInterface {
    username : string,
    password : string,
    sistemaCodigo : string    
}

export interface ResponseBodyLogin {
    trace:   Trace;
    status:  Status;
    payload: PayloadUsuario;
}

export interface ResponseBodyMenu {
    trace:   Trace;
    status:  Status;
    payload: PayloadMenu;
}

export interface ResponseBodyTipoDocumento {
    trace:   Trace;
    status:  Status;
    payload: TipoDocumento[];
}


export interface ResponseBodyCargo {
    trace:   Trace;
    status:  Status;
    payload: Cargo[];
}

export interface ResponseBodyTypeSupplier {
    trace:   Trace;
    status:  Status;
    payload: TipoProveedor[];
}

export interface ResponseBodyDepartamentos {
    trace:   Trace;
    status:  Status;
    payload: Departamento[];
}

export interface ResponseBodyProvincias {
    trace:   Trace;
    status:  Status;
    payload: Provincia[];
}

export interface ResponseBodyDistritos {
    trace:   Trace;
    status:  Status;
    payload: Distrito[];
}


export interface PayloadUsuario {   
    usuario: Usuario;
    menu:    Menu[];
}

export interface PayloadMenu{
    menu: Menu[];
}

export interface TipoDocumento{
    id : string;
    descripcion : string
}

export interface TipoProveedor{
    id : string;
    descripcion : string
}

export interface Cargo{
    id : string;
    descripcion : string
}

export interface Departamento{
    id : string;
    descripcion : string
}

export interface Provincia{
    id : string;
    descripcion : string
}
export interface Distrito{
    id : string;
    descripcion : string
}

export interface Menu {
    idMenu:      number;
    descripcion: string;
    url:         string;
    icono:       string;
    nivel:       string;
    orden:       string;
    submenu:     Menu[];
}

export interface Usuario {
    usuario:           string;
    idUsuario:         number;
    nombrePersona:     string;
    logo:              string;
    token:             string;
    cambiarPassword:   number;
}

export interface Status {
    success: boolean;
    error:   Error;
    mensaje: null;
}

export interface Error {
    code:     null;
    httpCode: null;
    messages: any[];
}

export interface Trace {
    traceId: null;
}
