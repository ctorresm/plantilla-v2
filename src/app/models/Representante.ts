import { Departamento, Provincia, Distrito } from "./UserInterface";
export class Representante {
  cargo: string;
  tipoDoc: string;
  documento: string;
  nombres: string;
  apellidos: string;
  telefono: string;
  anexo: string;
  extension: number;
  email: string; 
  constructor() {}
}
