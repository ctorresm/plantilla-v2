import { Trace, Status } from './UserInterface'

export interface estadoBandeja {
    id: string,
    descripcion: string
}

export interface ResponseBodyEstadoProveedor {
    trace: Trace;
    status: Status;
    payload: estadoBandeja[];
}