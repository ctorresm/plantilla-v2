import { Component, OnInit, Input } from '@angular/core';
import { SidenavService } from 'src/app/services/sidenav.service';
import { CapacitaService } from 'src/app/services/capacita.service';
import { faBuilding, faBorderAll, faGlobeAmericas } from '@fortawesome/free-solid-svg-icons';
import { Usuario, Menu } from 'src/app/models/UserInterface';
import { showNotificationMini } from 'src/app/utils/utilFunction';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})

export class SidenavComponent implements OnInit {

  faBuilding = faBuilding;
  faBorderAll = faBorderAll;
  faGlobeAmericas = faGlobeAmericas;
  usuario: Usuario;
  menuItems: Menu[];

  mostrar: boolean = true;
  @Input() movilSize: any;

  constructor(
    public _sidenavService: SidenavService,
    public _capacitaService: CapacitaService
  ) { }

  ngOnInit(): void {
    this.cargarMenu(1);
  }

  cambio() {
    this.mostrar = !this.mostrar
    this._sidenavService.toggle();
    this.mostrar ? this._sidenavService.icono = "fas fa-toggle-off" : this._sidenavService.icono = "fas fa-toggle-on"
  }

  private cargarMenu(userCodigo: number): void {
    try {
      this._capacitaService.obtenerMenuPorUsuarioId(userCodigo).subscribe(
        resp => {
          if (resp.status.success == true) {
            this.menuItems = resp.payload.menu;

    console.log(this.menuItems)

          } else {
            showNotificationMini(resp.status.mensaje, "error");
          }
        });
    } catch (error) { }
  }

}
