import { Component, OnInit, Input } from '@angular/core';
import { SidenavService } from 'src/app/services/sidenav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  abierto: boolean = true;
  @Input() movilSize: any;

  constructor(
    public _sidenavService: SidenavService
  ) { }

  ngOnInit() {
  }

  cambio() {
    this._sidenavService.toggle();
    this._sidenavService.sidenav.closedStart ? this.abierto = !this.abierto : null;
    this.abierto ? this._sidenavService.icono = "fas fa-toggle-off" : this._sidenavService.icono = "fas fa-toggle-on"

  }

}
