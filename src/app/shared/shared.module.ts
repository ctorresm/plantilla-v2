import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidenavComponent } from './sidenav/sidenav.component';
import { AngularMaterialComponents } from '../utils/material.module';



@NgModule({
    declarations: [
        FooterComponent,
        HeaderComponent,
        SidenavComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        AngularMaterialComponents,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        FooterComponent,
        SidenavComponent,
        HeaderComponent
    ],
    providers: [],
    entryComponents: []
})

export class SharedModule { }