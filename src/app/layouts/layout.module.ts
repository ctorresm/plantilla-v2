import { NgModule } from '@angular/core';

import { AuthLayoutComponent } from './auth-layout/auth-layout.component'
import { SharedModule } from '../shared/shared.module';
import { PagesLayoutComponent } from './pages-layout/pages-layout.component';
import { AngularMaterialComponents } from '../utils/material.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LoginModule } from './auth-layout/login/login.module';

@NgModule({
    declarations: [
        AuthLayoutComponent,
        PagesLayoutComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        HttpClientModule,
        SharedModule,
        AngularMaterialComponents,
        RouterModule,
        LoginModule
    ],
})

export class LayoutModule { }
