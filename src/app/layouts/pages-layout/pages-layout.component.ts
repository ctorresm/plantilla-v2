import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from 'src/app/services/sidenav.service';
import { MediaMatcher } from '@angular/cdk/layout';
declare var $: any;
@Component({
  selector: 'app-pages-layout',
  templateUrl: './pages-layout.component.html',
  styleUrls: ['./pages-layout.component.scss']
})
export class PagesLayoutComponent implements OnInit {

  mobileQuery: MediaQueryList;
  _mobileQueryListener: () => void;
  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(
    private _sidenavService: SidenavService,
    public changeDetector: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetector.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngAfterViewInit() {
    this._sidenavService.setSidenav(this.sidenav);
    this._sidenavService.close()
    this._sidenavService.open()
    this.changeDetector.detectChanges()
  }

  ngOnInit() {
    // $(".modal").appendTo("body");
  }

}
