import Swal from "sweetalert2";
import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { faUser, faUnlock } from "@fortawesome/free-solid-svg-icons";
import { ActivatedRoute, Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { showNotificationMini } from 'src/app/utils/utilFunction';
declare var $: any;
@Component({
  selector: "app-forget-password",
  templateUrl: "./forget-password.component.html",
  styleUrls: ["./forget-password.component.scss"]
})
export class ForgetPasswordComponent implements OnInit {
  @Input()
  display: boolean = false;
  @Output()
  close: EventEmitter<any> = new EventEmitter<any>();
  registerForm: FormGroup;
  submitted = false;
  faUser = faUser;
  faUnlock = faUnlock;
  displayLoginPasswordChangeModal: boolean = false;
  userEmail = "";
  isDivVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authoServ: LoginService
  ) {}

  closeLoginEntranceModal() {
    this.display = false;
    this.close.emit();
  }

  ngOnInit() {
    this.display = true;
    this.registerForm = this.formBuilder.group({
      userEmail: ["", [Validators.required, Validators.email]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  public forgotPassword(): void {
    this.submitted = true;
    this.isDivVisible = true;
    if (this.registerForm.valid) {
      this.authoServ.forgotPassword(this.userEmail).subscribe(
        response => {
          this.isDivVisible = false;         
          if (response.status.success == true) {
            var usuario = response.payload.usuario;
            this.authoServ.setUserToken(usuario);
            if (usuario.cambiarPassword == 1) {
              this.router.navigate(["/changepassword"]);
            } else {
              this.router.navigate([""]);
            }
          } else {
            showNotificationMini(response.status.mensaje, "error");
          }
        },
        error => {
          this.isDivVisible = false;
          showNotificationMini("error al conectarse al servidor", "error");
        }
      );
    } 
  }
  

  closePasswordChangeModalFromTop() {
    this.displayLoginPasswordChangeModal = false;
  }
}
