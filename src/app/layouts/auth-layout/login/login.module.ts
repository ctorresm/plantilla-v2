// ---------------- Modulos Angular ---------------- //
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// ---------------- Módulos ---------------- //
import { LoginRoutingModule } from "./login-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

// ---------------- Componentes ---------------- //
import { LoginComponent } from './login.component';
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { LoginEntranceModalComponent } from './login-entrance-modal/login-entrance-modal.component';
import { LoginPasswordChangeModalComponent } from "./login-password-change-modal/login-password-change-modal.component";
import { ForgetPasswordComponent } from "./forget-password/forget-password.component";
import { primeNgModule } from 'src/app/utils/primeng.module';


@NgModule({
  declarations: [
    ChangePasswordComponent,
    ForgetPasswordComponent,
    LoginPasswordChangeModalComponent,
    LoginComponent,
    LoginEntranceModalComponent
  ],
  imports: [
    primeNgModule,
    FontAwesomeModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LoginRoutingModule,
  ]
})
export class LoginModule { }
