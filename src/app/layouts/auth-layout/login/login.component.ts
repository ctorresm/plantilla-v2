import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})

export class LoginComponent implements OnInit {
  displayLoginEntranceModal: boolean = false;
  displayLoginPasswordChangeModal: boolean = false;

  constructor() {}

  ngOnInit() {}

  openLoginEntranceModal() {
    this.displayLoginEntranceModal = true;
  }

  closeEntranceModalFromTop() {
    this.displayLoginEntranceModal = false;
  }

  openLoginPasswordChangeModal() {
    this.displayLoginPasswordChangeModal = true;
  }

  closePasswordChangeModalFromTop() {
    this.displayLoginPasswordChangeModal = false;
  }
}
