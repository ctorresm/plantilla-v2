import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MustMatch } from "src/app/utils/validacion";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.css"]
})
export class ChangePasswordComponent implements OnInit {
  registerForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authoServ: LoginService
  ) {}

  displayLoginPasswordChangeModal: boolean = false;

  ngOnInit() {
    var usuario = this.authoServ.getCurrentUser();    
    if (usuario.cambiarPassword == 1) {
      this.displayLoginPasswordChangeModal = true;
    } else {
      this.router.navigate(["/cuadro-de-mando"]);
    }
  }

  openLoginPasswordChangeModal() {
    this.displayLoginPasswordChangeModal = true;
  }

  closePasswordChangeModalFromTop() {
    this.displayLoginPasswordChangeModal = false;
    this.router.navigate(["/login"]);
  }
}
