import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { faUser, faUnlock } from "@fortawesome/free-solid-svg-icons";
import { LoginService } from "src/app/services/login.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MustMatch } from "src/app/utils/validacion";
import { showNotificationMini } from "src/app/utils/utilFunction";
import { timer } from "rxjs";

@Component({
  selector: "app-login-password-change-modal",
  templateUrl: "./login-password-change-modal.component.html",
  styleUrls: ["./login-password-change-modal.component.css"]
})
export class LoginPasswordChangeModalComponent implements OnInit {
  @Input() display: boolean = false;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  faUser = faUser;
  faUnlock = faUnlock;
  registerForm: FormGroup;
  submitted = false;
  isDivVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authoServ: LoginService
  ) {}
  regexDni = "^[0-9]{8,8}$";

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        dni: ["", Validators.pattern(this.regexDni)],
        password: ["", [Validators.required, Validators.minLength(6)]],
        confirmPassword: ["", Validators.required]
      },
      {
        validator: MustMatch("password", "confirmPassword")
      }
    );
  }

  get f() {
    return this.registerForm.controls;
  }

  loginUser() {
    this.submitted = true;
    // stop here if form is invalid

    if (this.registerForm.valid) {
      var dni = "";
      var passwordOld = "";
      var passwordNew = "";
      var usuarioId = "";
      this.authoServ
        .changePassword(usuarioId, passwordOld, passwordNew)
        .subscribe(
          response => {
            this.isDivVisible = false;
            if (response.status.success == true) {
              showNotificationMini(response.status.mensaje, "success");
              const source = timer(2000);
              source.subscribe(val => {
                this.router.navigate(["/cuadro-de-mando"]);
              });
            } else {
              showNotificationMini(response.status.mensaje, "error");
            }
          },
          error => {
            this.isDivVisible = false;
            showNotificationMini("ERROR DE SERVIDOR", "error");
          }
        );

      return;
    }
  }

  closeLoginPasswordChangeModal() {
    this.display = false;
    this.close.emit();
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
