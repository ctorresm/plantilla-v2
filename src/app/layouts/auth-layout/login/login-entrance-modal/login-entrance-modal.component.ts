import Swal from "sweetalert2";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { faUser, faUnlock } from "@fortawesome/free-solid-svg-icons";
import { LoginService } from "src/app/services/login.service";
import { showNotificationMini } from "src/app/utils/utilFunction";
import { UserLoginInterface } from "src/app/models/UserInterface";
declare var $: any;

@Component({
  selector: "app-login-entrance-modal",
  templateUrl: "./login-entrance-modal.component.html",
  styleUrls: ["./login-entrance-modal.component.scss"]
})

export class LoginEntranceModalComponent implements OnInit {

  @Input() display: boolean = false;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  faUser = faUser;
  faUnlock = faUnlock;
  ApellidosNombres = "";
  displayLoginPasswordChangeModal: boolean = false;
  usuario: UserLoginInterface = {
    username: "",
    password: "",
    sistemaCodigo: ""
  };
  isDivVisible = false;
  constructor(
    private _Activatedroute: ActivatedRoute,
    private router: Router,
    private authoServ: LoginService
  ) { }

  closeLoginEntranceModal() {
    this.display = false;
    this.close.emit();
  }
  ngOnInit() {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    this.ApellidosNombres = this._Activatedroute.snapshot.paramMap.get(
      "ApellidosNombres"
    );
    if (this.ApellidosNombres != "" && this.ApellidosNombres != null) {
      //this.authoServ.setUser({ codigoUser: "", userName: "SIPGE13", email: "", password: "123456789", ApellidosNombres: this.ApellidosNombres })
      this.router.navigate([""]);
    }
  }

  public validarLogin(): void {
    this.isDivVisible = true;
    if (this.usuario.username == "ADMIN" && this.usuario.password == "12") {
      this.authoServ
        .loginUser(this.usuario.username, this.usuario.password)
        .subscribe(
          response => {
            this.isDivVisible = false;
            if (response.status.success == true) {
              var usuario = response.payload.usuario;
              this.authoServ.setUserToken(usuario);
              if (usuario.cambiarPassword == 1) {
                this.router.navigate(["/changepassword"]);
              } else {
                this.router.navigate(["/cuadro-de-mando"]);
              }
            } else {
              showNotificationMini(response.status.mensaje, "error");
            }
          },
          error => {
            this.isDivVisible = false;
            showNotificationMini("error al conectarse al servidor", "error");
          }
        );
    } else {
      showNotificationMini("USER Y/O PASSWORD INCORRECT", "error");
    }
  }
}
