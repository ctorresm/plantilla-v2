import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { PagesLayoutComponent } from './layouts/pages-layout/pages-layout.component';
import { LoginComponent } from './layouts/auth-layout/login/login.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [

  {
    path: '',
    component: PagesLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dependencias',
        pathMatch: 'full'
      },
      {
        path: 'cuadro-de-mando',
        loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'dependencias',
        loadChildren: () => import('./pages/dependencias/dependencias.module').then(m => m.DependenciasModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'bandeja',
        loadChildren: () => import('./pages/administrador-servir/bandeja-proveedores/bandeja-proveedores.module').then(m => m.BandejaProveedoresModule)
      }
    ]
  }, {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      }, {
        path: 'proveedor',
        loadChildren: () => import('./pages/proveedor/proveedor.module').then(m => m.ProveedorModule)
      },
      {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
