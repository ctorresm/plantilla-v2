import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  proveedor = {
    name: 'UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS',
    type: 'Universidad Pública',
    RUC: '1234567890',
    legalName: 'UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS',
    address: 'Calle Lima 123 - Lima, Lima, Lima',
    phoneNumber: '01 5555555',
    extension: '321',
    email: 'universidad@urp.edu.pe',
    webPage: 'https://urp.edu.pe'
  }

  representante = {
    documentType: 'DNI',
    documentNumber: '12345678',
    names: 'Elena Maria del Pozo Aliaga',
    phoneNumber: '01 8888888',
    extension: '654',
    email: 'edelpozo@urp.edu.pe'
  }

  encargado = {
    documentType: 'DNI',
    documentNumber: '87654321',
    names: 'Rosario Maritza Dominga Perez Vela',
    email: 'rperezv@urp.edu.pe'
  }

  constructor() { }

  ngOnInit() {
  }


}
