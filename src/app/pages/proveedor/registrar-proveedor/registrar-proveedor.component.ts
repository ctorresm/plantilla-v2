import { Component, OnInit } from '@angular/core';
import { showNotificationMini } from 'src/app/utils/utilFunction';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Representante } from 'src/app/models/Representante';
import { Distrito, Provincia, Departamento, Cargo, TipoProveedor, TipoDocumento } from 'src/app/models/UserInterface';
import { RequestRegistroProveedor } from 'src/app/models/Request';
import { Proveedor } from 'src/app/models/Proveedor';
import { CapacitaService } from 'src/app/services/capacita.service';

@Component({
  selector: 'app-registrar-proveedor',
  templateUrl: './registrar-proveedor.component.html',
  styleUrls: ['./registrar-proveedor.component.scss']
})
export class RegistrarProveedorComponent implements OnInit {
  constructor(
    private srvProvedor: CapacitaService,
    private formBuilder: FormBuilder
  ) { }

  proveedor = new Proveedor();
  representanteProv = new Representante();
  autoridadMax = new Representante();
  requestProveedor: RequestRegistroProveedor;
  registerForm: FormGroup;
  submitted = false;

  disabledSunat: boolean = false;

  lstTipoDoc: TipoDocumento[];
  lstTipoProveedor: TipoProveedor[];
  lstTipoCargo: Cargo[];
  lstDepartamento: Departamento[];
  lstProvincia: Provincia[];
  lstDistrito: Distrito[];

  //checkBoxTermino = false;
  displayModal = false;
  checkBoxTermino: string[] = [];
  checked: boolean = false;

  // var para cargar la imagen
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;

  //

  // funcion para subir la imagen

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    if (this.fileData) {
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(this.fileData);
      reader.onload = _event => {
        this.previewUrl = reader.result;
      };
    }
  }

  onSetValuesSunat() {
    debugger;
    this.proveedor.maxAutoridad = new Representante();
    this.proveedor.representante = new Representante();
    this.proveedor.provRUC = "107089483";
    this.proveedor.provRazonSocial = "RAZON SOCIAL DEMO";
    this.proveedor.provDirecion = "DIRECION DEMO";
    this.proveedor.maxAutoridad.nombres = "NOMBRE DE MAX AUTO";
    this.proveedor.maxAutoridad.apellidos = "apellido";
    this.proveedor.representante.apellidos = "REPRESENTANTE APELLIDO";
    this.registerForm.patchValue(this.proveedor);


  }

  onLoadImg() {
    const formData = new FormData();
    formData.append("file", this.fileData);
    // this.http.post('url/to/your/api', formData)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.uploadedFilePath = res.data.filePath;
    //     alert('SUCCESS !!');
    //   })
  }

  get f() {
    //console.log(this.registerForm.controls)
    return this.registerForm.controls;
  }

  get maxAutoridad() {
    return (<FormGroup>this.registerForm.get("maxAutoridad")).controls;
  }

  get representante() {
    return (<FormGroup>this.registerForm.get("representante")).controls;
  }

  onFormValidation() {
    this.registerForm = this.formBuilder.group({
      provRUC: ["", Validators.required],
      provRazonSocial: ["", Validators.required],
      provDirecion: ["", Validators.required],
      provDepartamento: ["", Validators.required],
      provProvincia: ["", Validators.required],
      provDistrito: ["", Validators.required],
      provTipoProveedor: ["", Validators.required],
      provTelefono: ["", Validators.required],
      provAnexo: ["", Validators.required],
      provPaginaWeb: ["", Validators.required],
      provAceptarTemino: ["", Validators.required],
      maxAutoridad: this.formBuilder.group({
        cargo: ["", Validators.required],
        tipoDoc: ["", Validators.required],
        documento: ["", Validators.required],
        nombres: ["", Validators.required],
        apellidos: ["", Validators.required],
        telefono: ["", Validators.required],
        anexo: ["", Validators.required],
        email: ["", Validators.required]
      }),
      representante: this.formBuilder.group({
        cargo: ["", Validators.required],
        tipoDoc: ["", Validators.required],
        documento: ["", Validators.required],
        nombres: ["", Validators.required],
        apellidos: ["", Validators.required],
        telefono: ["", Validators.required],
        anexo: ["", Validators.required],
        email: ["", Validators.required]
      })
    });
  }

  ngOnInit() {
    this.onFormValidation();
    this.representanteProv.tipoDoc = "-1";
    this.proveedor.provTipoProveedor = this.autoridadMax.tipoDoc = "-1";
    this.loadCombox();
  }

  public desahabilitarCampoSunat() {
    let provRazonSocial = this.registerForm.get("provRazonSocial").disable();
    let provDirecion = this.registerForm.get("provDirecion").disable();
    let provDepartamento = this.registerForm.get("provDepartamento").disable();
    let provProvincia = this.registerForm.get("provProvincia").disable();
    let provDistrito = this.registerForm.get("provDistrito").disable();
    //let maxAutoridad = this.registerForm.get("maxAutoridad").disable();
    let maxCargo = this.registerForm.get("maxAutoridad.cargo").disable();
    let provRUC = this.registerForm.get("provRUC").disable();
    this.onSetValuesSunat();

    alert(JSON.stringify(this.proveedor));

  }

  public habilitarCampoSunat() {
    let provRazonSocial = this.registerForm.get("provRazonSocial").enable();
    let provDirecion = this.registerForm.get("provDirecion").enable();
    let provDepartamento = this.registerForm.get("provDepartamento").enable();
    let provProvincia = this.registerForm.get("provProvincia").enable();
    let provDistrito = this.registerForm.get("provDistrito").enable();
    //let maxAutoridad = this.registerForm.get("maxAutoridad").enable();
    let maxCargo = this.registerForm.get("maxAutoridad.cargo").enable();
    let provRUC = this.registerForm.get("provRUC").disable();
  }

  public saveSuppliers(): void {
    this.submitted = true;

    if (this.registerForm.valid) {
      this.registerForm.controls[""];
      this.requestProveedor.proveedor = this.registerForm.value;
      this.requestProveedor.representantes.push(this.representanteProv);
      this.requestProveedor.representantes.push(this.autoridadMax);
    }
    console.log(this.registerForm.controls);
  }

  public loadCombox(): void {
    this.getAllTypeDocuments();
    this.getAllPositions();
    this.getAllTypeSuppliers();
    this.getDepartamentos();
  }

  public getDistritoByProvincia(e): void {
    try {
      var provi = e.target.value;
      var depa = this.registerForm.get("provDepartamento").value;
      this.srvProvedor
        .getDistritos(depa, provi)
        .subscribe(mc => {
          if (mc.status.success) {
            this.lstDistrito = mc.payload;
          } else {
            showNotificationMini(mc.status.mensaje, "error");
          }
        });
    } catch (error) {
      console.log(error);
    }
  }

  public getProvinciaByDepartamento(e): void {
    try {
      debugger;
      var departamento = e.target.value;
      this.srvProvedor.getProvincias(departamento).subscribe(mc => {
        if (mc.status.success) {
          this.lstProvincia = mc.payload;
        } else {
          showNotificationMini(mc.status.mensaje, "error");
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public getDepartamentos(): void {
    try {
      this.srvProvedor.getDepartamentos().subscribe(resp => {
        if (resp.status.success) {
          this.lstDepartamento = resp.payload;
        } else {
          showNotificationMini(resp.status.mensaje, "error");
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public getAllTypeSuppliers(): void {
    try {
      this.srvProvedor.getTypeSuppliers().subscribe(resp => {
        if (resp.status.success) {
          this.lstTipoProveedor = resp.payload;
        } else {
          showNotificationMini(resp.status.mensaje, "error");
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public getAllPositions(): void {
    try {
      this.srvProvedor.getTypePosition().subscribe(resp => {
        if (resp.status.success) {
          this.lstTipoCargo = resp.payload;
        } else {
          showNotificationMini(resp.status.mensaje, "error");
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public getAllTypeDocuments(): void {
    try {
      this.srvProvedor.getTypeDocuments().subscribe(resp => {
        if (resp.status.success) {
          this.lstTipoDoc = resp.payload;
        } else {
          showNotificationMini(resp.status.mensaje, "error");
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  openModal(): void {
    this.checked = false;
    this.displayModal = true;
  }

  aceptarTermino(): void {
    this.checked = true;
    this.displayModal = false;
  }

  cancelarTermino(): void {
    this.checked = false;
    this.displayModal = false;
  }
}
