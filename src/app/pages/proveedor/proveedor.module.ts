import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProveedorRoutingModule } from './proveedor-routing.module';
import { AngularMaterialComponents } from 'src/app/utils/material.module';
import { primeNgModule } from 'src/app/utils/primeng.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProveedorRoutingModule,
    AngularMaterialComponents,
    primeNgModule
  ]
})
export class ProveedorModule { }
