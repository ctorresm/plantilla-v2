import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrarProveedorComponent } from './registrar-proveedor/registrar-proveedor.component';


const routes: Routes = [
  {
    path: 'registrar',
    component: RegistrarProveedorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProveedorRoutingModule { }
