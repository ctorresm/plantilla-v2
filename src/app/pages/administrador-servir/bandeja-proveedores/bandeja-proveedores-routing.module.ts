import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BandejaProveedoresComponent } from './bandeja-proveedores.component';
import { DetalleProveedorComponent } from './detalle-proveedor/detalle-proveedor.component';

const routes: Routes = [
  { path: '', component: BandejaProveedoresComponent },
  { path: ':id', component: DetalleProveedorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BandejaProveedoresRoutingModule { }
