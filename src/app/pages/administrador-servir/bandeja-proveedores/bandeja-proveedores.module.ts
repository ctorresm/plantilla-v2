import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BandejaProveedoresRoutingModule } from './bandeja-proveedores-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { primeNgModule } from 'src/app/utils/primeng.module';
import { BandejaProveedoresComponent } from './bandeja-proveedores.component';
import { DetalleProveedorComponent } from './detalle-proveedor/detalle-proveedor.component';


@NgModule({
  declarations: [
    BandejaProveedoresComponent,
    DetalleProveedorComponent
  ],
  imports: [
    CommonModule,
    BandejaProveedoresRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    primeNgModule
  ]
})
export class BandejaProveedoresModule { }
