import { Component, OnInit } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CapacitaService } from 'src/app/services/capacita.service';
import { BandejaService } from 'src/app/services/bandeja.service';
import { Router } from '@angular/router';
import { ResponseBodyEstadoProveedor } from 'src/app/models/bandejaAdministrador';
import { ResponseBodyTypeSupplier, ResponseBodyDepartamentos, ResponseBodyProvincias, ResponseBodyDistritos } from 'src/app/models/UserInterface';

@Component({
  selector: 'app-bandeja-proveedores',
  templateUrl: './bandeja-proveedores.component.html',
  styleUrls: ['./bandeja-proveedores.component.scss']
})

export class BandejaProveedoresComponent implements OnInit {

  provincias = []
  departamentos = []
  distritos = []
  tipoProveedores = []
  estadosProveedor = []
  faSearch = faSearch;
  paginatorCount = ''
  results = [];
  cargando: boolean = false;

  bandejaProveedoresForm: FormGroup = this.fb.group({
    inputBuscar: [''],
    fechaInicio: [''],
    fechaFin: [''],
    estado: [''],
    tipoProveedor: [''],
    departamento: [''],
    provincia: [''],
    distrito: ['']
  })

  data = [
    { id: 'C-01', ruc: '23456787898', proveedor: 'Universidad Santa Ana Sur', tipoProveedor: 'Universidad Privada', departamento: 'Lima', estado: { value: 1, descripcion: 'Nueva' } },
    { id: 'C-02', ruc: '23456787898', proveedor: 'Instituto La molina Este', tipoProveedor: 'Instituto Privado', departamento: 'Apurimac', estado: { value: 2, descripcion: 'Observado' } },
    { id: 'C-03', ruc: '23456787898', proveedor: 'Unversidad La molina Este', tipoProveedor: 'Universidad Publica', departamento: 'Lima', estado: { value: 2, descripcion: 'Observado' } },
    { id: 'C-04', ruc: '23456787898', proveedor: 'UNMSM', tipoProveedor: 'Universidad Publica', departamento: 'Lima', estado: { value: 3, descripcion: 'Aprobado' } },
  ]

  constructor(
    private _capacitaService: CapacitaService,
    private _bandejaService: BandejaService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.listarEstadoProveedores();
    this.listarTipoProveedores();
    this.listarDepartamentos();
  }

  search(event) {
    this.results = []
    this._bandejaService.obtenerProveedoresFiltro(event.query).subscribe(
      (res: any) => {
        this.results = res.payload
      },
      err => {
        console.log(err);
      }
    )
  }

  listarEstadoProveedores() {
    this._bandejaService.obtenerEstadosBandeja().subscribe(
      (res: ResponseBodyEstadoProveedor) => {
        this.estadosProveedor = res.payload
      },
      error => { console.log(error) }
    )
  }

  listarTipoProveedores() {
    this._capacitaService.getTypeSuppliers().subscribe(
      (res: ResponseBodyTypeSupplier) => {
        this.tipoProveedores = res.payload;
      },
      error => { console.log(error) }
    )
  }

  listarDepartamentos() {
    this._capacitaService.getDepartamentos().subscribe(
      (res: ResponseBodyDepartamentos) => {
        this.departamentos = res.payload;
      },
      error => { console.log(error) }
    )
  }

  cambioDepartamento(valueDep: string) {
    this.listarProvincias(valueDep)
  }

  cambioProvincia(valueProv: string) {
    this.listarDistritos(this.bandejaProveedoresForm.controls['departamento'].value, valueProv);
  }

  listarProvincias(depa: string) {
    this.provincias = []
    this._capacitaService.getProvincias(depa).subscribe(
      (res: ResponseBodyProvincias) => {
        this.provincias = res.payload
      },
      error => { console.log(error) }
    )
  }

  listarDistritos(depa: string, prov: string) {
    this._capacitaService.getDistritos(depa, prov).subscribe(
      (res: ResponseBodyDistritos) => {
        this.distritos = res.payload;
      },
      error => { console.log(error) }
    )
  }

  buscarProveedores() {
    console.log(this.bandejaProveedoresForm.value)
  }

  verDetalle(idProveedor: string) {
    this.router.navigateByUrl(`/bandeja/${idProveedor}`)
  }

}
