import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DependenciasRoutingModule } from './dependencias-routing.module';
import { DependenciasComponent } from './dependencias.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { primeNgModule } from 'src/app/utils/primeng.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DependenciasComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DependenciasRoutingModule,
    FontAwesomeModule,
    primeNgModule
  ]
})
export class DependenciasModule { }
