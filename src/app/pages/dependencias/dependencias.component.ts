import { Component, OnInit } from '@angular/core';
import { faSearch, faEraser, faPlusCircle, faEye, faPencilAlt, faTrashAlt, faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-dependencias',
  templateUrl: './dependencias.component.html',
  styleUrls: ['./dependencias.component.scss']
})
export class DependenciasComponent implements OnInit {
  academicFields: [] = []
  academicDepartment: [] = []
  dep: [] = []
  prov: [] = []
  city: [] = []
  registerLocationModal: boolean = false

  // iconos
  faSearch = faSearch
  faEraser = faEraser
  faPlusCircle = faPlusCircle
  faEye = faEye
  faPencilAlt = faPencilAlt
  faTrashAlt = faTrashAlt
  faCloudDownloadAlt = faCloudDownloadAlt

  data = [
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' },
    { location: 'Facultad de administración', address: 'Calle Lima 123', dep: 'Lima', prov: 'Lima', city: 'Miraflores' }
  ]

  constructor() { }

  ngOnInit() {
  }

  validateAcademicField() {

  }

  validateAcademicDepartment() {

  }

  registerLocation() {
    this.registerLocationModal = true
  }


}
