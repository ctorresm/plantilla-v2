import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DependenciasComponent } from './dependencias.component';


const routes: Routes = [
  { 
    path: '',
    component: DependenciasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DependenciasRoutingModule { }
