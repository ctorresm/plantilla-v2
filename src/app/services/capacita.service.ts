import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import {
  ResponseBodyTipoDocumento,
  ResponseBodyTypeSupplier,
  ResponseBodyCargo,
  ResponseBodyDepartamentos,
  ResponseBodyProvincias,
  ResponseBodyDistritos
} from "./../models/UserInterface";
import {
  ResponseBodyLogin,
  Usuario,
  ResponseBodyMenu
} from "../models/UserInterface";

import {
  urlSrvLogin,
  urlBaseMock,
  SISTEMA_ID
} from "src/environments/constante";
import { isNullOrUndefined } from "util";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class CapacitaService {
  constructor(private http: HttpClient) {}

  public obtenerMenuPorUsuarioId(
    idUsuario: number
  ): Observable<ResponseBodyMenu> {
    return this.http.get<ResponseBodyMenu>(
      urlBaseMock + "getMenuForUser/" + idUsuario
    );
  }

  public getTypeDocuments(): Observable<ResponseBodyTipoDocumento> {
    return this.http.get<ResponseBodyTipoDocumento>(
      urlBaseMock + "capacitaMas/api/v1/generales/tipoDocumentos"
    );
  }

  public getTypeSuppliers(): Observable<ResponseBodyTypeSupplier> {
    return this.http.get<ResponseBodyTypeSupplier>(
      urlBaseMock + "capacitaMas/api/v1/generales/tipoProveedores"
    );
  }

  public getTypePosition(): Observable<ResponseBodyCargo> {
    return this.http.get<ResponseBodyCargo>(
      urlBaseMock + "capacitaMas/api/v1/generales/tipoCargos"
    );
  }

  public getDepartamentos(): Observable<ResponseBodyDepartamentos> {
    return this.http.get<ResponseBodyDepartamentos>(
      urlBaseMock + "capacitaMas/api/v1/departamentos"
    );
  }

  public getProvincias(depa: string): Observable<ResponseBodyProvincias> {
    // debugger;
    return this.http.get<ResponseBodyProvincias>(
      // urlBaseMock + "capacitaMas/api/v1/provincias/depa/" + depa
      urlBaseMock + "capacitaMas/api/v1/provincias/depa/3"
    );
  }

  public getDistritos(
    depa: string,
    prov: string
  ): Observable<ResponseBodyDistritos> {
    return this.http.get<ResponseBodyDistritos>(
      // urlBaseMock +
      //   "capacitaMas/api/v1/distritos/prov/" +
      //   prov +
      //   "/depa/" +
      //   depa

      urlBaseMock +
        "capacitaMas/api/v1/distritos/prov/64/depa/01"
    );
  }
}
