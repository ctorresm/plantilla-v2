import { Injectable } from '@angular/core';
import { urlBaseMock } from 'src/environments/constante';
import { Observable } from 'rxjs';
import { ResponseBodyEstadoProveedor } from '../models/bandejaAdministrador';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BandejaService {

  constructor(
    private http: HttpClient
  ) { }

  public obtenerEstadosBandeja(): Observable<ResponseBodyEstadoProveedor> {
    return this.http.get<ResponseBodyEstadoProveedor>(urlBaseMock + "capacitaMas/api/v1/proveedores/estados");
  }

  public obtenerProveedoresFiltro(frase: string): Observable<any> {
    return this.http.get<any>(urlBaseMock + "capacitaMas/api/v1/proveedores/estados");
  }

  public validarProveedor(id: string): Observable<any> {
    return this.http.get<any>(urlBaseMock + "capacitaMas/api/v1/proveedores/estados");
  }

  public ingresarObservacion(id: string): Observable<any> {
    return this.http.get<any>(urlBaseMock + "capacitaMas/api/v1/proveedores/estados");
  }

}




