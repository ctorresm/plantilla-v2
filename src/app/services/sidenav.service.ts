import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root'
})

export class SidenavService {

  public icono: string = 'fas fa-toggle-off'
  public sidenav: MatSidenav;
  public mostrar: boolean = false;

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  public open() {
    return this.sidenav.open();
  }

  public close() {
    return this.sidenav.close();
  }

  public toggle(): void {
    if (this.sidenav.opened == false) {
      this.sidenav.toggle();
      this.mostrar = true
    }
    else {
      this.mostrar = !this.mostrar
    }
  }



}
