import { Injectable } from "@angular/core";
import { ResponseBodyLogin, Usuario } from "../models/UserInterface";
import { parametro } from "src/environments/parametro";
import { Observable, of } from "rxjs";
import {
  urlSrvLogin,
  usuarioDummy,
  urlBaseMock,
  SISTEMA_ID
} from "src/environments/constante";
import { isNullOrUndefined } from "util";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class LoginService {
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });
  constructor(private http: HttpClient) {}

  public onLogoutUser(): void {
    localStorage.removeItem("currentUser");
    //return this.http.post<UserInterface>(urlSrvLogin, { headers: this.headers });
  }

  public loginUser(
    username: string,
    password: string
  ): Observable<ResponseBodyLogin> {
    let serv = "authentication";
    var parameter = new parametro();
    parameter.strCampo1 = username;
    parameter.strCampo2 = password;
    return this.http
      .post<ResponseBodyLogin>(urlBaseMock + "/login", {
        headers: this.headers
      })
      .pipe(map(data => data));
  }

  public setUserToken(user: Usuario): void {
    debugger;
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  public getCurrentUser(): Usuario {
    let user_string = localStorage.getItem("currentUser");
    if (!isNullOrUndefined(user_string)) {
      let user: Usuario = JSON.parse(user_string);
      return user;
    } else {
      return null;
    }
  }

  public changePassword(
    idUsuario: string,
    passwordOld: string,
    passwordNew: string
  ) {
    let parameter = {
      idUsuario: idUsuario,
      passwordOld: passwordOld,
      passwordNew: passwordNew,
      sistemaId: SISTEMA_ID
    };
    return this.http
      .put<ResponseBodyLogin>(urlBaseMock + "changepassword", parameter, {
        headers: this.headers
      })
      .pipe(map(data => data));
  }

  public forgotPassword(email: string): Observable<ResponseBodyLogin> {
    return null;
  }
}
