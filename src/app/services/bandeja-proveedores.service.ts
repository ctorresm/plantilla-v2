import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlBaseMock } from 'src/environments/constante';

@Injectable({
  providedIn: 'root'
})
export class BandejaProveedoresService {

  constructor(
    private http: HttpClient
  ) { }

  public obtenerProveedoresFiltro(frase: string): Observable<any> {
    return this.http.get<any>(urlBaseMock + "capacitaMas/api/v1/proveedores/estados");
  }



}
