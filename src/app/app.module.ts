import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";

import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { primeNgModule } from './utils/primeng.module';
import { LayoutModule } from './layouts/layout.module';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegistrarProveedorComponent } from './pages/proveedor/registrar-proveedor/registrar-proveedor.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    RegistrarProveedorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    AppRoutingModule,
    primeNgModule,
    LayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
