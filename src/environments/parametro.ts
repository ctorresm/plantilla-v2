export class parametro {
  public strCampo1: string = "";
  public strCampo2: string = "";
  public strCampo3: string = "";
  public strCampo4: string = "";
  public intCampo1: number = 0;
  public dtCampo1: Date;
  public blnCampo1: boolean;
  public dtCampo2: Date;
  public blnCampo2: boolean;
  public dtCampo3: Date;
  public blnCampo3: boolean;

  parametro() {
    this.strCampo1 = "";
    this.strCampo2 = "";
    this.strCampo3 = "";
    this.strCampo4 = "";
    this.intCampo1 = 0;
  }
}
