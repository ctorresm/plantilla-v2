//export const urlService = "http://ws.inpe.gob.pe/Reniec/ConsultaMQ/identificacion.asmx";
//export const urlSrvLogin = "http://10.4.0.38/ws_scai/ws_sys_login.asmx/";
export const urlService =
  "http://10.4.0.38:85/web_api/api/DocumentoSecretario/";
export const urlSrvLogin = "http://10.4.0.38:85/web_api/api/login/";
export const urlBaseMock = "https://demo7288088.mockable.io/";
//export const urlService = "http://localhost:4561//api/DocumentoSecretario/";
//export const urlSrvLogin = "http://localhost:4561//api/login/";
export const CRUD = 1;
export const SELECT = 2;
export const SISTEMA_ID = 1;
export const usuarioDummy: any = {
  ApellidosNombres: "",
  Rol: "1",
  NombreRol: "",
  SMS_ERROR: "error al logearse"
};
export const listaSistema: any[] = [
  {
    cardtitle: "Sistema de Registro de población penitenciaria",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/imagen_personas.jpg",
    tipo: "1",
    prioridad: 2
  },

  {
    cardtitle: "Sistema de tratamiento",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/imagen_cubo.jpg",
    tipo: "1",
    prioridad: 1
  },
  {
    cardtitle: "Sistema de declaración visitantes",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/loguin_Logo.jpg",
    tipo: "1",
    prioridad: 2
  },
  {
    cardtitle: "Sistema de Recursos Humanos",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/imagen_rrhh.png",
    tipo: "2",
    prioridad: 5
  },
  {
    cardtitle: "Sistema de  Resoluciones",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/imagen_normas.jpg",
    tipo: "2",
    prioridad: 3
  },
  {
    cardtitle: "Sistema GeoReferencial",
    carddescription: "Registro de visitantes de los centros penitenciarios",
    price: "2019",
    cardcategory: "Lima - San Juan",
    img: "./assets/img/sistemas/geo_referencial.png",
    tipo: "2",
    prioridad: 2
  }
];
