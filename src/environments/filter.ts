import { Injectable, Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterBy"
})
@Injectable()
export class FilterByPipe implements PipeTransform {
  transform(array: any[], filter: any): any {
    if (!array) {
      return array;
    }
    switch (typeof filter) {
      case "boolean":
        return array.filter(this.filterByBoolean(filter));
      case "number":
        if (FilterByPipe.isNumber(filter)) {
          var arr = array.filter(function(sistema) {
            return sistema.tipo == filter;
          });
          return arr;
        }
      case "function":
        return array.filter(filter);
    }
    return array.filter(this.filterDefault(filter));
  }

  static isNumber(value) {
    return !isNaN(parseInt(value, 10)) && isFinite(value);
  }

  private filterByString(filter) {
    if (filter) {
      filter = filter.toLowerCase();
    }
    return value =>
      !filter ||
      (value ? ("" + value).toLowerCase().indexOf(filter) !== -1 : false);
  }

  private filterByBoolean(filter) {
    return value => Boolean(value) === filter;
  }

  private filterDefault(filter: any): (value: any) => boolean {
    return (value: any) => filter === undefined || filter == value;
  }
}
